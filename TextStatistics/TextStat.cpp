#include "TextStat.h"


bool TextStat::isDuplicate(std::map<int, int> list, int posX, int posY, int minX, int minY){
	typedef std::map<int, int>::iterator it_type;
	for (it_type iterator = list.begin(); iterator != list.end(); iterator++) {
		int x = iterator->first;
		int y = iterator->second;

		if (posX != x && std::abs(posX - x) < minX && std::abs(posY - y) < minY){
			return true;
		}
	}

	return false;
}

Score TextStat::match(const string templ, int width, float threshold)
{
	// Load the template image
	templateImage = imread(templ, 0);

	// Resize the template to the desired size
	float aspectRatio = (float)templateImage.cols / templateImage.rows;
	int height = (float)width / aspectRatio;
	resize(templateImage, templateImage, Size(width, height));

	// Convert template image to grayscale
	Mat templf;
	templateImage.convertTo(templf, CV_32FC1);

	// Convert image to grayscale
	Mat grayImg = imread(fileName, 0);
	Mat grayImgf;
	grayImg.convertTo(grayImgf, CV_32FC1);

	/// Create the result matrix
	int result_cols = grayImg.cols - templateImage.cols + 1;
	int result_rows = grayImg.rows - templateImage.rows + 1;
	Mat result;
	result.create(result_rows, result_cols, CV_32FC1);

	// Perform template matching
	matchTemplate(grayImg, templateImage, result, TM_CCOEFF_NORMED);

	// Vector to store the norm between the template and the current patch
	vector<double> distances;

	// Set min/max x and y values for the current template
	float minX = templateImage.cols;
	float minY = templateImage.rows;

	// Create a map to store match locations
	std::map<int, int> locs;
	int locSize = 0;

	// Store matches and prevent storing duplicates
	for (int i = 0; i < result.rows; i++){
		for (int j = 0; j < result.cols; j++){
			if (result.at<float>(i, j) > threshold){
				char letter = templ[templ.find_last_of("_") + 1];

				locs.insert(std::make_pair(j, i));

				if (locs.size() > locSize){

					bool add = true;

					if (!isDuplicate(locs, j, i, minX, minY)){
						Mat temp;
						temp.create(templateImage.rows, templateImage.cols, CV_32FC1);
						for (int k = 0; k < templateImage.rows; k++){
							for (int l = 0; l < templateImage.cols; l++){
								temp.at<float>(k, l) = grayImgf.at<float>(i + k, j + l);
							}
						}
						distances.push_back(norm(temp, templf));
					}

					locSize++;
				}
			}
		}
	}

	// Sort the norms
	std::sort(distances.begin(), distances.end());

	// Calculate mean distance
	float mean = 0.0;
	for (int i = 0; i < distances.size(); i++){
		mean += distances[i];
	}
	mean /= distances.size();

	return Score(width, mean, distances.size());
}

vector<Score> TextStat::bulkMatch(const string templ, float threshold){
	vector<Score> result;
	for (int i = 5; i < 25; i++){
		Score score = match(templ, i, threshold);
		if (score.score > 0){
			result.push_back(match(templ, i, threshold));
		}
	}
	std::sort(result.begin(), result.end());
	return result;
}

Score* TextStat::finalFilter(std::vector<Score>& scores){
	if (scores.size() > 0){
		return &scores[0];
	}
	else{
		return NULL;
	}
}

void TextStat::printImageSize(Mat image){
	std::cout << image.cols << "x" << image.rows << std::endl;
}