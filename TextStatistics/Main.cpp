#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <map>
#include "TextStat.h"

using namespace cv;

int main(int, char** argv)
{

	// Put your image in the Samples folder and change the name of the image in the url below to use your own image. Or uncomment
	//	one of the lines below to try it with the sample files.
	TextStat ts("Samples/sample1.png");
	//TextStat ts("Samples/sample2.png");

	// First letter template in the alphabet (letter a)
	string templName = "Templates/Times New Roman/lower_a";
	string format = ".jpg";

	int letterCount = 0;
	std::map<std::string, int> charMap;
	int averageLetterCount = 0;

	std::cout << "Progress: " << std::endl;
	for (int i = 97; i < 123; i++){
		std::cout << std::floor((((float)i - 97) / (123 - 97)) * 100) << "% ..." << std::endl;

		// Set the name of the template
		templName.erase(templName.size() - 1, 1);
		string letter = string(1, i);
		templName.append(letter);
		string fullTemplName = string(templName);
		fullTemplName.append(format);

		// Get matches from a low and high threshold respectively
		std::vector<Score> lowThreshScores = ts.bulkMatch(fullTemplName, 0.65);
		std::vector<Score> highThreshScores = ts.bulkMatch(fullTemplName, 0.9);

		// Get the highest scores from the two thresholds
		Score* highThreshResult = ts.finalFilter(highThreshScores);
		Score* lowThreshResult = ts.finalFilter(lowThreshScores);

		// Choose one of the results
		bool chooseHighThreshResult;
		bool bothResultsNull = false;

		if (lowThreshResult == NULL && highThreshResult == NULL){
			bothResultsNull = true;
		}
		else if (highThreshResult == NULL){
			chooseHighThreshResult = false;
		}
		else if (lowThreshResult == NULL){
			chooseHighThreshResult = true;
		}
		else{
			if (lowThreshResult->score < highThreshResult->score && lowThreshResult->size > highThreshResult->size && std::abs(lowThreshResult->size - highThreshResult->size) <= 10){
					
				chooseHighThreshResult = false;
			}
			else{
				chooseHighThreshResult = true;
			}
		}

		if (bothResultsNull){
			charMap.insert(std::pair<std::string, int>(letter, 0));
		}
		else if (chooseHighThreshResult){
			charMap.insert(std::pair<std::string, int>(letter, highThreshResult->size));

			averageLetterCount += highThreshResult->size;
		}
		else{
			charMap.insert(std::pair<std::string, int>(letter, lowThreshResult->size));
			averageLetterCount += lowThreshResult->size;
		}
	}

	std::cout << "100% ..." << std::endl << std::endl;

	std::cout << "RESULTS:" << std::endl << std::endl;

	if (charMap.size() > 0){

		averageLetterCount /= charMap.size();

		typedef std::map<std::string, int>::iterator it_type;
		for (it_type iterator = charMap.begin(); iterator != charMap.end(); iterator++) {
			std::string letter = iterator->first;
			int count = iterator->second;

			if (std::abs(count - averageLetterCount) > 50){
				count = 0;
			}

			letterCount += count;
		}
	}

	std::cout << "Approximate Letter Count: " << letterCount << std::endl;

	float averageLettersPerWordInEnglish = 4.5;
	int numberOfWords = letterCount / averageLettersPerWordInEnglish;

	std::cout << "Approximate Word Count: " << numberOfWords << std::endl;

	std::cout << "\nFINISHED" << std::endl;
	cv::waitKey(0);
	return 0;
}
