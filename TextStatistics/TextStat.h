#ifndef TEXT_STAT_H
#define TEXT_STAT_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <vector>

using namespace cv;

// Represents the score of a match using a specific template width
struct Score{
	// Width of the template
	int width;

	// The score of the match
	float score;

	// The number of matches
	int size;

	Score(int width, float score, int size) : width(width), score(score), size(size){}

	bool operator < (const Score& other) const
	{
		return (score < other.score);
	}

	void print(){
		std::cout << width << " " << score << " " << size << std::endl;
	}
};

class TextStat{

public:

	TextStat(const char* fileName) : fileName(fileName){
		image = imread(fileName, -1);
		namedWindow(imageWindow, WINDOW_AUTOSIZE);
		imshow(imageWindow, image);

		maxTrackbar = 50;
		width = 20;
	}

	/*
	Helper function to print the size of an image for debugging.
	@param: image the image.
	*/
	void printImageSize(Mat image);

	/*
	Matches the given template with the source image.
	@Param: templ the path of the template image.
	@param: width the width of the template image.
	@param: threshold the desired threshold. Higher threshold gives better accuracy.
	@return: the score of the match.
	*/
	Score match(string templ, int width, float threshold);

	/*
	Checks if a match has already been found.
	@param: list the list of matches.
	@param: posX the x position of the match in the image.
	@param: posY the y position of the match in the image.
	@param: minX the minimum x coordinate for the match to be considered a duplicate.
	@param: minY the minimum y coordinate for the match to be considered a duplicate.
	@return: true if the match at position posX and posY is a duplicate. Otherwise, returns false.
	*/
	bool isDuplicate(std::map<int, int> list, int posX, int posY, int minX, int minY);

	/*
	Launches a series of matches with different template sizes and find the best match.
	@param: templ the path of the template image.
	@param: threshold the required precision threshold
	@return: the best match for this template.
	*/
	vector<Score> bulkMatch(const string templ, float threshold);

	/*
	Filters all the matches for a single template and returns the best match
	@param: scores the list of scores for one template
	@return: a pointer to the best score
	*/
	Score* finalFilter(std::vector<Score>& scores);

	inline int* getWidth() { return &width; }
	inline const int getMaxTrackbar() const{ return maxTrackbar; }
	inline const char* getImageWindow() const{ return imageWindow; }

private:
	// The source image
	Mat image;

	// The template image used for template matching
	Mat templateImage;

	// Name of file to be read
	const char* fileName;

	// Title of the window
	const char* imageWindow = "Source Image";

	// Maximum value that trackbar can have
	int maxTrackbar;

	// Initial width value of trackbar
	int width;
};

#endif