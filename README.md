**Overview**

This is my computer vision app developed using C++ and OpenCV. The purpose of this app is to take as input an image of a text written in Times New Roman font, and output an approximation of the total letter and word count. This is done by using template matching to detect all the letters and count their occurrences.   

**Setup Instructions**

To try this app, do the following:

1. Put your text image in the "Samples" folder located in the "TextStatistics" folder.

2. Open the project in Visual Studio.

3. Navigate to the main class.

4. In the main function, locate the TextStat (ts) object (it's the first line of code in the main function), and change the url to
match the name of your text image.

5. Run the application. The progress and results will be displayed in the window that appears. A second window is created which displays the source image at the end to help you verify the word/letter count if necessary. 

**Notes**

* This app uses template matching to identify and count the letters. It also uses a double thresholding system. It uses a high and low threshold and outputs the best results based on a mixture of accuracy of matches and the number of matches found.

* For now this app only works with the Times New Roman font and lower case letters. However, this can be
easily extended to work with different fonts and upper-case letters by adding the necessary font templates in the "Templates" folder. 

* Better accuracy is obtained when the font is not too small. Good examples are in the "Samples" folder. 

* If the input image is too big, it might take some time to output results.

* The results produced by this app are not exact, the app does its best to output a precise estimation of the letter and word count.